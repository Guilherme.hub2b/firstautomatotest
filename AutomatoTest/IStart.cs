﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomatoTest
{
    public interface IStart
    {
        void Run();
    }
}
