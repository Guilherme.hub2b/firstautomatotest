﻿using System;

namespace AutomatoTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var automato = new Start();
            automato.Run();
        }
    }
}
