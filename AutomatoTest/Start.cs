﻿using AutomatoTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace AutomatoTest
{
    public class Start : IStart
    {
        private List<Cell> AliveCells { get; set; }
        private List<Fruit> Fruits { get; set; }
        private List<Monster> Monsters { get; set; }
        private int RoundBorns { get; set; }
        private int RoundDeaths { get; set; }
        private int RoundTeleportedCells { get; set; }
        private int RoundCount { get; set; }

        private int MaxFruits = 200;
        private int MaxMonsters = 200;

        private int CanvasLimit = 20000;
        public void Run()
        {
            AliveCells = InitializeCells();
            Fruits = InitializeFruits();
            Monsters = InitializeMonsters();
            
            Console.WriteLine("Id - PosX - PosY");
            foreach (var cell in AliveCells)
                Console.WriteLine($"{cell.Id} - {cell.PosX} - {cell.PosY}");
            int count = AliveCells.Count;

            while (count != 0)
            {
                if (Monsters.Count() > MaxMonsters)
                    Monsters = Monsters.GetRange(0, MaxMonsters);

                if (Fruits.Count() > MaxFruits)
                    Fruits = Fruits.GetRange(0, MaxFruits);

                RoundCount++;
                RoundBorns = RoundDeaths = RoundTeleportedCells = 0;
                AliveCells = Walk(AliveCells);
                count = AliveCells.Count;
                Console.WriteLine($"[{RoundCount}] Alive: {AliveCells.Count} | Borns: {RoundBorns} | Monster Deaths: {RoundDeaths} | TeleportedCells: {RoundTeleportedCells}");
               // Thread.Sleep(10);
            }

            AliveCells = Walk(AliveCells);

            Console.ReadLine();
        }

        private List<Cell> InitializeCells()
        {
            var cells = new List<Cell>();
            Random random = new Random();
            int initialQuantity = 200;
            int count = 0;

            while (count <= initialQuantity)
            {
                cells.Add(new Cell(Guid.NewGuid())
                {
                    PosX = random.Next(0, CanvasLimit + 1),
                    PosY = random.Next(0, CanvasLimit + 1)
                });
                count++;
            }
            return cells;
        }

        private List<Monster> InitializeMonsters()
        {
            var Monsters = new List<Monster>();
            Random random = new Random();
            int initialQuantity =  MaxMonsters;
            int count = 0;

            while (count <= initialQuantity)
            {
                Monsters.Add(new Monster(Guid.NewGuid())
                {
                    PosX = random.Next(0, CanvasLimit + 1),
                    PosY = random.Next(0, CanvasLimit + 1)
                });
                count++;
            }
            return Monsters;
        }

        private List<Fruit> InitializeFruits()
        {
            var Fruits = new List<Fruit>();
            Random random = new Random();
            int initialQuantity = MaxFruits;
            int count = 0;

            while (count <= initialQuantity)
            {
                Fruits.Add(new Fruit(Guid.NewGuid())
                {
                    PosX = random.Next(0, CanvasLimit + 1),
                    PosY = random.Next(0, CanvasLimit + 1)
                });
                count++;
            }
            return Fruits;
        }
        private List<Cell> Walk(List<Cell> cells)
        {
            Random random = new Random();

            var cellsResult = cells;
            foreach (var interator in cells.ToArray())
            {
                var cell = cellsResult.FirstOrDefault(c => c.Id == interator.Id);
                cell = WalkCell(interator);

                if (cell.PosY > CanvasLimit)
                    cell.PosY = random.Next(0, CanvasLimit);
                if (cell.PosX > CanvasLimit)
                    cell.PosX = random.Next(0, CanvasLimit);

                cell.PosX = Math.Abs(cell.PosX);
                cell.PosY = Math.Abs(cell.PosY);

                if (AliveCells.Where(c => c.PosX == cell.PosX && c.Id != cell.Id).Count() > 0 || AliveCells.Where(c => c.PosY == cell.PosY && c.Id != cell.Id).Count() > 0)
                {
                    RoundTeleportedCells++;
                    cell = TeleportCell(cell);
                    continue;
                }
                if (Monsters.Where(m => m.PosX == cell.PosX).Count() > 0 && Monsters.Where(m => m.PosY == cell.PosY).Count() > 0)
                {
                    Monsters.Remove(Monsters.FirstOrDefault(m => m.PosX == cell.PosX && m.PosY == cell.PosY));
                    AddMonster();
                    RoundDeaths++;
                    cellsResult.Remove(cell);
                    continue;
                }

                else if (Fruits.Where(f => f.PosX == cell.PosX).Count() > 0 && Fruits.Where(f => f.PosY == cell.PosY).Count() > 0)
                {
                    RoundBorns++;
                    cellsResult.Add(AddCell());
                    Fruits.Remove(Fruits.FirstOrDefault(f => f.PosX == cell.PosX && f.PosY == cell.PosY));
                    AddFruit();
                    continue;
                }

            }

            return cellsResult;
        }

        private Cell WalkCell(Cell cell)
        {
            Random random = new Random();
            cell.PosX += random.Next(-1, 1);
            cell.PosY += random.Next(-1, 1);
            return cell;
        }

        private void AddFruit()
        {
            Random random = new Random();

            Fruits.Add(new Fruit(Guid.NewGuid())
            {
                PosX = random.Next(0, CanvasLimit),
                PosY = random.Next(0, CanvasLimit)
            });
        }

        private void AddMonster()
        {
            Random random = new Random();

            Monsters.Add(new Monster(Guid.NewGuid())
            {
                PosX = random.Next(0, CanvasLimit),
                PosY = random.Next(0, CanvasLimit)
            });
        }
        private Cell AddCell()
        {
            Random random = new Random();

            return new Cell(Guid.NewGuid())
            {
                PosX = random.Next(0, CanvasLimit),
                PosY = random.Next(0, CanvasLimit)
            };
        }

        private Cell TeleportCell(Cell cell)
        {
            Random random = new Random();
            cell.PosX += random.Next(0, CanvasLimit);
            cell.PosY += random.Next(0, CanvasLimit);
            return cell;
        }
    }
}
