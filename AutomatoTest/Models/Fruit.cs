﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomatoTest.Models
{
    public class Fruit
    {
        public Fruit(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
    }
}
